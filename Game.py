from asyncio import ensure_future
from enum import Enum
from math import floor
from random import shuffle, choice

from Configuration import *
from Localization import *
from Util import make_sleep, letter_generator, gen_ordinal, send_message


class Game:
    """Represents a game of TWOW."""
    def __init__(self, starter, channel, client, cleanup_function):
        self._cleanup = cleanup_function
        self._recieved_prompt = False
        self._start_now = False
        self._all_responses_in = False
        self._all_votes_in = False
        self._timer_controller = make_sleep()
        self._stopped = False
        self._tasks = []

        self._channel = channel
        self._client = client

        self._has_prompted = set()
        self._has_responded = set()

        self._prompts = []
        self._responses = {}
        self._rounds = -1
        self._maxletter = ""
        self._voters = set()
        self._scores = {}
        self._round_scores = {}
        self._placements = {}

        self._prompter_time = DEFAULT_PROMPTER_TIME
        self._response_time = DEFAULT_RESPONSE_TIME
        self._vote_time = DEFAULT_VOTE_TIME

        self.server = channel.server
        self.starter = starter
        self.userlist = {starter}
        self.gamestage = GameStage.LOADING
        self.prompter = starter

        self._tasks.append(ensure_future(self.start()))

    def get_current_round(self):
        """Gets the current roudn of the game."""
        return len(self._has_prompted)

    def add(self, usr):
        """Adds a user to the game."""
        self.userlist.add(usr)
        print(usr.name + " Joined!")

    def remove(self, usr):
        """Removes a user from the game."""
        if self.prompter == usr and self.gamestage == GameStage.PROMPTING:
            self._tasks.append(ensure_future(
                send_message(self._client, 
                    self._channel, GAME_PROMPTER_LEFT.replace("@PROMPTER", self.prompter.mention)
                )))
        self.userlist.remove(usr)

    def kick(self, usr, reason):
        """Kicks a user off the game."""
        self.remove(usr)
        self._tasks.append(ensure_future(send_message(self._client, usr, KICK_TEXT.replace("@REASON", reason))))

    async def start(self, timeout=DEFAULT_STARTING_SECONDS):
        """Kicks off the game with the JOINING phase!"""
        timeleft = timeout
        self.gamestage = GameStage.JOINING
        self._tasks.append(ensure_future(
            send_message(self._client, self._channel, GAME_TIME_START.replace(
                "@TIME", str(timeleft) + (" second" if timeleft == 1 else " seconds"))
                         )))
        # Begin Countdown
        for _ in range(JOIN_TIME_WARNINGS):
            if self._stopped:
                return
            if not self._start_now:
                self._tasks.append(ensure_future(
                    send_message(self._client, self._channel,
                                              GAME_TIME.replace("@TIME", str(floor(timeleft))
                                                               + (" second" if floor(timeleft) == 1 else " seconds"))
                                              )))
                await self._timer_controller(timeleft / JOIN_TIME_WARNINGS)
                timeleft -= timeout / JOIN_TIME_WARNINGS
        # Check minimum player requirement
        self._start_now = False
        if len(self.userlist) < MINIMUM_PLAYERS_REQUIRED:
            await send_message(self._client, self._channel,
                                            ERROR_NOT_ENOUGH_PLAYERS.replace("@PLAYERS", str(MINIMUM_PLAYERS_REQUIRED))
                               )
            await self.stop()
            return
        else:
            for usr in self.userlist:
                self._scores[usr] = 0
            await send_message(self._client, self._channel, GAME_START, type_after=True)
            await send_message(self._client, self._channel,
                                            GAME_PLAYERS.replace("@USERLIST", ', '.join(
                                               map(lambda u: u.mention, self.userlist))),
                               type_after=True)
        self._rounds = len(self.userlist)
        self._tasks.append(ensure_future(self.round()))

    async def round(self, prompt_timeout=DEFAULT_PROMPTER_TIME, response_timeout=DEFAULT_RESPONSE_TIME,
                    vote_timeout=DEFAULT_VOTE_TIME):
        """Represents one round of TWOW."""
        # Round and Prompter
        self._has_prompted.add(self.prompter)
        self._tasks.append(ensure_future(
            send_message(self._client, self.prompter,
                                      GAME_HELP_PROMPTER
                                      .replace("@PROMPTER", self.prompter.mention)
                                      .replace("@TIME", str(self._prompter_time) + " seconds")
                         )))
        await send_message(self._client, self._channel,
                           GAME_ROUND
                           .replace("@ROUND", str(self.get_current_round()))
                           .replace("@PROMPTER", self.prompter.mention),
                           type_after=True
                           )
        self.gamestage = GameStage.PROMPTING

        # Prompting Timer
        timeleft = prompt_timeout
        for _ in range(PROMPT_TIME_WARNINGS):
            if self._stopped:
                return
            if not self._recieved_prompt:
                self._tasks.append(ensure_future(
                    send_message(self._client, self._channel,
                                 GAME_TIME
                                 .replace("@TIME", str(floor(timeleft))
                                          + (" second" if floor(timeleft) == 1 else " seconds"))
                                 )))
                await self._timer_controller(timeleft / PROMPT_TIME_WARNINGS)
                timeleft -= prompt_timeout / PROMPT_TIME_WARNINGS

        # After received or timer runs out
        if not self._recieved_prompt:
            await self.kick(self.prompter, KICK_REASON_INACTIVITY)
            await self.next()
            return

        self._recieved_prompt = False
        prompt = self._prompts[len(self._prompts) - 1]

        # Display the prompt
        await send_message(self._client, self._channel, GAME_GOT_PROMPT, type_after=True)
        await send_message(self._client, self._channel, GAME_DISPLAY_PROMPT.replace("@PROMPT", prompt), type_after=True)
        self.gamestage = GameStage.RESPONDING
        self._responses[self.get_current_round()] = []

        # Get responses going out
        responders = self.userlist.copy()
        responders.remove(self.prompter)
        for responder in responders:
            self._tasks.append(ensure_future(
                send_message(self._client, responder,
                                          GAME_HELP_RESPONSE
                                          .replace("@RESPONDER", responder.mention)
                                          .replace("@TIME", str(floor(response_timeout)) + " seconds")
                                          )))
            self._tasks.append(ensure_future(send_message(self._client, responder, GAME_DISPLAY_PROMPT.replace("@PROMPT", prompt))))
        await send_message(self._client, self.prompter, GAME_PROMPTER_WAIT)

        # Response Timing
        timeleft = response_timeout
        for _ in range(RESPONSE_TIME_WARNINGS):
            if self._stopped:
                return
            if not self._all_responses_in:
                self._tasks.append(ensure_future(
                    send_message(self._client, self._channel,
                                 GAME_TIME
                                 .replace("@TIME", str(floor(timeleft))
                                          + (" second" if floor(timeleft) == 1 else " seconds"))
                                 )))
                await self._timer_controller(timeleft / RESPONSE_TIME_WARNINGS)
                timeleft -= prompt_timeout / RESPONSE_TIME_WARNINGS
        await send_message(self._client, self._channel, GAME_GOT_ALL_RESPONSES, type_after=True)
        self._all_responses_in = False

        # Did we get any responses?
        if len(self._responses[self.get_current_round()]) == 0:
            await send_message(self._client, self._channel, ERROR_NO_RESPONSES, type_after=True)
            await self.next()
            return

        # Display all responses!
        shuffle(self._responses[self.get_current_round()])
        random_responses = self._responses[self.get_current_round()]
        lettergen = letter_generator()
        resp_str = ""
        for res in random_responses:
            self._maxletter = next(lettergen)
            res.set_letter(self._maxletter)
            resp_str += GAME_RESPONSE_DISPLAY.replace('@LETTER', res.letter).replace('@RESPONSE', res.content)

        await send_message(self._client, self._channel, resp_str)
        self.gamestage = GameStage.VOTING

        # Begin Voting!
        await send_message(self._client, self._channel, GAME_VOTE_READY)
        for u in self.userlist:
            self._tasks.append(ensure_future(send_message(self._client, u, GAME_HELP_VOTE)))
            self._tasks.append(ensure_future(send_message(self._client, u, resp_str)))

        # Wait for votes...
        timeleft = vote_timeout
        for _ in range(VOTE_TIME_WARNINGS):
            if self._stopped:
                return
            if not self._all_votes_in:
                self._tasks.append(ensure_future(
                    send_message(self._client, self._channel,
                                              GAME_TIME
                                              .replace("@TIME", str(floor(timeleft))
                                                      + (" second" if floor(timeleft) == 1 else " seconds"))
                                              )))
                await self._timer_controller(timeleft / VOTE_TIME_WARNINGS)
                timeleft -= vote_timeout / VOTE_TIME_WARNINGS
        self._all_votes_in = False

        # Results
        if len(self._voters) != 0:
            await send_message(self._client, self._channel, GAME_GOT_ALL_VOTES, type_after=True)
            await send_message(self._client, self._channel, GAME_DISPLAY_RANKING, type_after=True)
            ranking = sorted(self._responses[self.get_current_round()], key=lambda c: c.score, reverse=False)
            i = 1
            round_scores = ""
            last = None
            for resp in ranking:
                self._round_scores[resp.submitter] = resp.score
                self._scores[resp.submitter] += resp.score
                if i == 1:
                    round_scores += (GAME_ROUND_WINNER
                                     .replace("@PLACE", str(i))
                                     .replace("@RESPONSE", resp.content)
                                     .replace("@USER", resp.submitter.mention)
                                     .replace("@SCORE", str(resp.score))
                                     .replace("@LETTER", resp.letter)
                                     + "\n" + GAME_ROUND_OTHERS + "\n")
                else:
                    round_scores += (GAME_ROUND_RANKING_NORMAL
                                     .replace("@PLACE", str(i))
                                     .replace("@RESPONSE", resp.content)
                                     .replace("@USER", resp.submitter.mention)
                                     .replace("@SCORE", str(resp.score))
                                     .replace("@LETTER", resp.letter))
                i += 1

            await send_message(self._client, self._channel, round_scores, type_after=True)
            await self.prune()
            await send_message(self._client, self._channel, GAME_ROUND_END, type_after=True)
            self._tasks.append(ensure_future(self.next()))
            return
        else:
            await send_message(self._client, self._channel, ERROR_NO_VOTES, type_after=True)
            output = ""
            for resp in self._responses[self.get_current_round()]:
                output += (GAME_ROUND_RANKING_NO_VOTE
                           .replace("@RESPONSE", resp.content)
                           .replace("@USER", resp.submitter.mention)
                           .replace("@LETTER", resp.letter))
            await send_message(self._client, self._channel, output, type_after=True)
            self._tasks.append(ensure_future(self.next()))
            return

    async def next(self):
        """Goes to the next round, or stops the game and shows final scores if appropriate."""
        possibilities = list(filter(lambda e: e not in self._has_prompted, self.userlist))
        print(', '.join(map(str, possibilities)))
        print(', '.join(map(str, self.userlist)))
        print(', '.join(map(str, self._has_responded)))
        print(', '.join(map(str, self._has_prompted)))
        print(', '.join(map(lambda t: t[0].name + " w/ " + str(t[1]), self.get_sorted_placements())))

        if len(possibilities) < 1:
            self._has_prompted.clear()
            possibilities = self.userlist.copy()

        if len(self.userlist) < 3:
            await send_message(self._client, self._channel, GAME_END, type_after=True)
            await send_message(self._client, self._channel, GAME_FINAL_RESULTS, type_after=True)
            final_players = []
            for user in self.userlist:
                final_players.append(user)
            final_players = sorted(final_players,
                                   key=lambda u:self._scores[u] if u in self._has_responded else 0xffffffff,
                                   reverse=False)
            i = len(final_players)
            for user in final_players:
                self._placements[user] = i
                i -= 1
            # TODO: Only given a placement when you're eliminated. Add final 2 users appropriately.
            places_final_sorted = self.get_sorted_placements()
            final_output = ""
            for place in places_final_sorted:
                if place[0] in self._has_responded:
                    final_output += (GAME_FINAL_RESULTS_ENTRY.replace("@PLACE", str(place[1]))
                        .replace("@USER", place[0].mention)
                        .replace("@SCORE", str(self._scores[place[0]])))
            await send_message(self._client, self._channel, final_output)
            ensure_future(self.stop())
            return
        else:
            self.prompter = choice(possibilities)
            self._timer_controller.cancel_all()
            self.gamestage = GameStage.LOADING
            self._maxletter = ""
            self._voters.clear()
            self._tasks.append(ensure_future(self.round()))
            return

    async def stop(self):
        """Stops the game."""
        self._stopped = True
        ensure_future(self._cleanup(self._channel.server, self._channel))
        self._timer_controller.cancel_all()
        for task in self._tasks:
            task.cancel()

    async def receive_prompt(self, prompt, usr):
        """Recieves the prompt from the user."""
        self._prompts.append(prompt)
        self._recieved_prompt = True
        self._timer_controller.cancel_all()
        await send_message(self._client, usr, GAME_RECEIVED_DM)

    async def receive_response(self, response, usr):
        """Receives a response from a user."""
        allow = True
        for r in self._responses[self.get_current_round()]:
            if r.submitter == response.submitter:
                allow = False
                break
        if allow:
            self._responses[self.get_current_round()].append(response)
            self._has_responded.add(usr)
            await send_message(self._client, usr, GAME_RECEIVED_DM)

        if len(self._responses[self.get_current_round()]) == len(self.userlist) - 1:
            self._all_responses_in = True
            self._timer_controller.cancel_all()

    async def receive_vote(self, vote_string, usr):
        """Receives a vote in string format, and checks it for validity."""
        vote_string = vote_string.upper()
        if len(vote_string) == (ord(self._maxletter) - ord('A') + 1) and usr not in self._voters:
            for c in vote_string:
                if ord(c) > ord(self._maxletter):
                    await send_message(self._client, usr, ERROR_INVALID_VOTE)
                    return
            i = 1
            votes = [self.get_response_by_letter(l) for l in vote_string]
            for vote in votes:
                vote.receive_score(i)
                i += 1
            self._voters.add(usr)
            await send_message(self._client, usr, GAME_RECEIVED_DM)
        else:
            await send_message(self._client, usr, ERROR_INVALID_VOTE)

        if len(self._voters) == len(self.userlist):
            self._all_votes_in = True
            self._timer_controller.cancel_all()

    def get_response_by_letter(self, letter):
        """Gets the response based on the letter."""
        for res in self._responses[self.get_current_round()]:
            if res.letter == letter:
                return res

    async def force_start(self):
        """Forces the game to start right away."""
        if self.gamestage == GameStage.JOINING:
            self._start_now = True
            self._timer_controller.cancel_all()

    def get_sorted_placements(self):
        """Gets placements, sorted from best to worst"""
        scores = []
        for usr in self._placements:
            if usr in self._has_responded:
                scores.append((usr, self._placements[usr]))

        return sorted(scores, key=lambda t: t[1], reverse=False)

    def get_sorted_round_scores(self):
        """Gets scores, sorted from best to worst"""
        scores = []
        for usr in self._round_scores:
            if usr in self._has_responded:
                scores.append((usr, self._scores[usr]))

        return sorted(scores, key=lambda t: t[1], reverse=False)

    async def prune(self):
        """Determines which contestants to eliminate."""
        scores_sorted = list(filter(lambda t:
                                    t[0] in self.userlist and t[0] != self.prompter,
                                    reversed(self.get_sorted_round_scores())
                                    ))
        num_contestants_alive = len(scores_sorted)
        num_contestants_to_prune = max(1, int(floor(PRUNE_RATIO * num_contestants_alive)))
        for eliminee in scores_sorted[0: num_contestants_to_prune]:
            await self.byebye(eliminee[0])

    async def byebye(self, eliminee):
        """Formally eliminates an eliminee"""
        self._placements[eliminee] = len(self.userlist)
        self.remove(eliminee)
        await send_message(self._client, eliminee,
                           GAME_ELIMINATED.replace("@ORDINAL", gen_ordinal(self._placements[eliminee])))
        await send_message(self._client, self._channel,
                           GAME_PUBLIC_ELIMINATION
                           .replace("@LUSER", eliminee.mention)
                           .replace("@PERCENT", str(floor(PRUNE_RATIO * 100)) + "%")
                           )

    def __eq__(self, other):
        if isinstance(other, Game):
            return self._channel == other._channel
        else:
            return NotImplemented


class GameList(dict):
    """A list of games and the servers/channels they are in."""
    def add(self, server, channel, game):
        if server not in self:
            self[server] = dict()
            self[server][channel] = game
        elif not self.get_game(server, channel):
            self[server][channel] = game

    def get_game(self, server, channel):
        if server not in self or channel not in self[server]:
            return None
        else:
            return self[server][channel]

    def remove(self, server, channel):
        if self.get_game(server, channel):
            del self[server][channel]
            self[server][channel] = None
        else:
            raise KeyError

    def __str__(self):
        x = ""
        for server in self:
            x += "```" + server.name + "```\n"
            for channel in self[server]:
                x += channel.mention + " - " + str(bool(self.get_game(server, channel)))
        return x


class UserList(dict):
    """A list of users and the games they are in."""
    def add(self, user, game):
        self[user] = game

    def get_game(self, user):
        if user not in self or self[user] is None:
            return None
        else:
            return self[user]

    def remove(self, user):
        if user in self:
            self[user] = None
        else:
            raise KeyError


class Response:
    """Represents a response to a prompt."""
    def __init__(self, submitter, content):
        self.content = content
        self.votes = 0
        self.submitter = submitter
        self.letter = 'nil'
        self.score = 0

    def set_letter(self, letter):
        """Sets the letter that corresponds to this response in the votes."""
        self.letter = letter

    def receive_score(self, score):
        """Receives a score, keeping the average."""
        self.votes += 1
        self.score = (self.score + score) / self.votes


class GameStage(Enum):
    """Simple enum that keeps track of the game stage."""
    LOADING = -1
    JOINING = 0
    PROMPTING = 1
    RESPONDING = 2
    VOTING = 3
