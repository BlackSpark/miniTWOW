import discord

import Game

client = ...  # type: discord.Client
games = ...  # type: Game.GameList
users = ...  # type: Game.UserList


@client.event
async def on_ready(): ...


@client.event
async def on_message(msg: discord.Message): ...

async def remove_game(server: discord.Server, channel: discord.Channel): ...

def check_permissions(server: discord.Server, user: discord.User, game: Game.Game) -> bool: ...