"""Contains some utility functions"""
import asyncio

# Thanks https://stackoverflow.com/questions/37209864/interrupt-all-asyncio-sleep-currently-executing
def make_sleep():
    """Creates a sleep Task that is cancellable."""
    async def sleep(delay, result=None, *, loop=None):
        """Sleep Task"""
        coro = asyncio.sleep(delay, result=result, loop=loop)
        task = asyncio.ensure_future(coro)
        sleep.tasks.add(task)
        try:
            return await task
        except asyncio.CancelledError:
            return result
        finally:
            sleep.tasks.remove(task)

    sleep.tasks = set()

    def canceller():
        """Cancelling Function"""
        print("--> Cancelling all sleeping tasks")
        return sum(task.cancel() for task in sleep.tasks)

    sleep.cancel_all = canceller
    return sleep


# Thanks https://stackoverflow.com/questions/9647202/ordinal-numbers-replacement
def gen_ordinal(n):
    """Generates ordenal numbers"""
    return "%d%s" % (n, "tsnrhtdd"[(n // 10 % 10 != 1) * (n % 10 < 4) * n % 10::4])


def letter_generator():
    """Generates letters sequentially, like in an Excel spreadsheet."""
    A = ord('A')
    Z = ord('Z')
    letters = [A]
    while True:
        ret = ''.join([chr(n) for n in reversed(letters[:])])
        if len(ret) > 1:
            ret = '0' + ret
        yield ret
        letters[0] += 1
        n = 0
        while letters[n] > Z:
            letters[n] = A
            n += 1
            if n >= len(letters):
                letters.append(A)
                break
            else:
                letters[n] += 1

async def send_message(client, dest, message, type_after=False):
    await client.send_typing(dest)
    await client.send_message(dest, message)
    if type_after:
        await client.send_typing(dest)
