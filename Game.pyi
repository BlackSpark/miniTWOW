import sched
from enum import Enum
from typing import List, Callable, Any, Dict, Tuple, Set
import asyncio

import discord

from Configuration import DEFAULT_STARTING_SECONDS

s = ... # type: sched.scheduler

class Game:
    def __init__(self, starter: discord.User, channel: discord.Channel, client: discord.Client,
                 cleanup_function: Callable):
        self._cleanup = ...
        self._recieved_prompt = ... # type: bool
        self._start_now = ... # type: bool
        self._all_responses_in = ... # type: bool
        self._all_votes_in = ... # type: bool
        self._timer_controller = ...
        self._stopped = ... # type: bool
        self._tasks = ... # type: List[asyncio.Task]

        self._channel = ... # type: discord.Channel
        self._client = ... # type: discord.Client

        self._has_prompted = ... # type: Set[discord.User]
        self._has_responded = ... # type: Set[discord.User]

        self._prompts = ... # type: List[str]
        self._responses = ... # type: Dict[int, List[Response]]
        self._rounds = ... # type: int
        self._maxletter = ... # type: str
        self._voters = ... # type: Set[discord.User]
        self._scores = ... # type: Dict[discord.User, float]
        self._round_scores = ... # type: Dict[discord.User, float]
        self._placements = ... # type: Dict[discord.User, int]

        self._prompter_time = ... # type: int
        self._response_time = ... # type: int
        self._vote_time = ... # type: int

        self.server = ... # type: discord.Server
        self.starter = ... # type: discord.User
        self.userlist = ... # type: Set[discord.User]
        self.gamestage = ... # type: GameStage
        self.prompter = ... # type: discord.User

        ...

    def get_current_round(self) -> int: ...

    def add(self, usr: discord.User): ...

    def remove(self, usr: discord.User): ...

    async def kick(self, usr: discord.User, reason: str): ...

    async def start(self, timeout: int=DEFAULT_STARTING_SECONDS): ...

    async def round(self): ...

    async def next(self): ...

    async def stop(self): ...

    async def receive_prompt(self, prompt: str, usr: discord.User): ...

    async def receive_response(self, response: Response, usr: discord.User): ...

    async def receive_vote(self, vote_string: str, usr: discord.User): ...

    def get_response_by_letter(self, letter: str) -> Response: ...

    async def force_start(self): ...

    def get_sorted_placements(self) -> List[Tuple[discord.User, int]]: ...

    def get_sorted_round_scores(self) -> List[Tuple[discord.User, float]]: ...

    def prune(self): ...

    async def byebye(self, eliminee: discord.User): ...

    def __eq__(self, other: Any) -> bool: ...


class GameList(dict):
    def add(self, server: discord.Server, channel: discord.Channel, game: Game): ...

    def get_game(self, server: discord.Server, channel: discord.Channel) -> Game: ...

    def remove(self, server: discord.Server, channel: discord.Channel): ...

    def __str__(self) -> str: ...


class UserList(dict):
    def add(self, user: discord.User, game: Game):

    def get_game(self, user: discord.User) -> Game: ...

    def remove(self, user: discord.User): ...


class Response:
    def __init__(self, submitter: discord.User, content: str):
        self.content = ... # type: str
        self.votes = ... # type: int
        self.submitter = ... # type: discord.User
        self.letter = ... # type: str
        self.score = ... # type: float
        ...

    def set_letter(self, letter: str): ...

    def receive_score(self, score: int): ...

class GameStage(Enum):
    LOADING = -1
    JOINING = 0
    PROMPTING = 1
    RESPONDING = 2
    VOTING = 3