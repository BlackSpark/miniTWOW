from typing import Generator, Any
import discord

def make_sleep(): ...

def gen_ordinal(n: int) -> str: ...

def letter_generator() -> Generator: ...

async def send_message(client: discord.Client, dest: Any, message: str, type_after: bool=False): ...