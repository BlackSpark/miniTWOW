import logging
import re
from asyncio import ensure_future
import discord

import secret
from Game import UserList, GameList, Game, GameStage, Response
from Localization import *
from Configuration import TWOW_ADMIN_ROLE_NAME

client = discord.Client()
logger = logging.getLogger('discord')
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)

games = GameList()
users = UserList()


@client.event
async def on_ready():
    print('Logged in!')
    print(client.user.name)
    print(client.user.id)


@client.event
async def on_message(msg):
    """Handles all the messaging, and communicates with the Game."""
    global games
    match = re.match("^" + COMMAND_BASE + " ?(.*)", msg.content)

    # COMMAND INTERPRETATION #
    if msg.author != client.user and match:
        # Start
        if match.group(1).lower() == COMMAND_START:
            if not msg.channel.is_private and games.get_game(msg.server, msg.channel) is None:
                ensure_future(client.send_message(msg.author, MESSAGE_JOINED.replace("@SERVER", msg.server.name)))
                ensure_future(
                    client.send_message(msg.channel, MESSAGE_GAME_STARTED.replace("@AUTHOR", msg.author.mention)))
                ensure_future(client.send_message(msg.channel, MESSAGE_HELP_JOIN))
                new_game = Game(msg.author, msg.channel, client, remove_game)
                games.add(msg.server, msg.channel, new_game)
                users.add(msg.author, new_game)
            elif msg.channel.is_private:
                ensure_future(client.send_message(msg.channel, ERROR_DM))
            elif games.get_game(msg.server, msg.channel) is not None:
                ensure_future(client.send_message(msg.channel, ERROR_GAME_RUNNING))
        # Stop
        elif match.group(1).lower() == COMMAND_STOP:
            if not msg.channel.is_private and games.get_game(msg.server, msg.channel) is not None:
                game = games.get_game(msg.server, msg.channel)
                if check_permissions(msg.server, msg.author, game):
                    ensure_future(
                        client.send_message(msg.channel, MESSAGE_GAME_STOPPED.replace("@AUTHOR", msg.author.mention)))
                    ensure_future(games.get_game(msg.server, msg.channel).stop())
                else:
                    ensure_future(client.send_message(msg.author, ERROR_NOT_PRIVILEGED))
            elif msg.channel.is_private:
                ensure_future(client.send_message(msg.channel, ERROR_DM))
            elif games.get_game(msg.server, msg.channel) is None:
                ensure_future(client.send_message(msg.channel, ERROR_NO_GAME_RUNNING))
        # Join
        elif match.group(1).lower() == COMMAND_JOIN:
            if not msg.channel.is_private and games.get_game(msg.server, msg.channel) is not None \
                    and users.get_game(msg.author) is None:
                users.add(msg.author, games.get_game(msg.server, msg.channel))
                games.get_game(msg.server, msg.channel).add(msg.author)
                ensure_future(client.send_message(msg.author, MESSAGE_JOINED.replace("@SERVER", msg.server.name)))
            elif msg.channel.is_private:
                ensure_future(client.send_message(msg.channel, ERROR_DM))
            elif games.get_game(msg.server, msg.channel) is None:
                ensure_future(client.send_message(msg.author,
                                                  ERROR_NO_GAME_RUNNING_ON_SERVER.replace("@SERVER", msg.server.name)))
            elif users.get_game(msg.author) is not None:
                ensure_future(client.send_message(msg.author,
                                                  ERROR_ALREADY_JOINED.replace("@SERVER",
                                                                               users.get_game(msg.author).server.name)))
        # Leave
        elif match.group(1).lower() == COMMAND_LEAVE:
            if users.get_game(msg.author):
                ensure_future(client.send_message(msg.author, MESSAGE_LEFT.replace("@SERVER",
                                                                                   users.get_game(
                                                                                       msg.author).server.name)))
                users.remove(msg.author)
            elif msg.author not in users:
                ensure_future(client.send_message(msg.author, ERROR_NO_GAME_JOINED))
        # List
        elif match.group(1).lower() == COMMAND_LIST:
            if not msg.channel.is_private:
                ensure_future(client.send_message(msg.channel, str(games)))
            else:
                ensure_future(client.send_message(msg.channel, ERROR_DM))
        # Force
        elif match.group(1).lower() == COMMAND_FORCE:
            game = games.get_game(msg.server, msg.channel)
            if check_permissions(msg.server, msg.author, game) and game is not None:
                ensure_future(game.force_start())
            elif game is None:
                ensure_future(client.send_message(msg.author, ERROR_NO_GAME_RUNNING_ON_SERVER))
            else:
                ensure_future(client.send_message(msg.author, ERROR_NOT_PRIVILEGED))
        # Help
        else:
            ensure_future(client.send_message(msg.channel, MESSAGE_HELP))

    # GAME MANAGEMENT (DMs) #
    if msg.channel.is_private and users.get_game(msg.author):
        game = users.get_game(msg.author)
        # Get Prompt
        if game.prompter == msg.author and game.gamestage == GameStage.PROMPTING:
            ensure_future(game.receive_prompt(msg.content, msg.author))
        # Get Response
        elif msg.author != game.prompter and game.gamestage == GameStage.RESPONDING:
            ensure_future(game.receive_response(Response(msg.author, msg.content), msg.author))
        # Get Vote
        elif game.gamestage == GameStage.VOTING:
            ensure_future(game.receive_vote(msg.content, msg.author))


async def remove_game(server, channel):
    for user in games.get_game(server, channel).userlist:
        users.remove(user)
        ensure_future(client.send_message(user, GAME_ENDED_DM.replace("@SERVER", server.name)))
    games.remove(server, channel)


def check_permissions(server, user, game):
    member = server.get_member(user.id)
    if member is not None:
        for role in member.roles:
            if role.name == TWOW_ADMIN_ROLE_NAME:
                return True
    return user == game.starter


# Starts the program
client.run(secret.api_key)
